<?php namespace Tests\Feature\Api\Rest\Traits;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 28/11/17
 * Time: 22:43
 */
trait RestfullResourceTestTrait {
    use DatabaseTransactions;

    protected $queryModel;
    protected $route;

    public function setup() {
        parent::setup();
        $this->havingIdealCenarioForTests();
    }

    /**
     * Initialize the $queryModel property and defines the ideal cenario for running this test
     * @return mixed/
     */
    abstract function havingIdealCenarioForTests();

    /**
     * Returns an array of data ready to be used as a input for create a new resource
     * @return array
     */
    abstract function providerFieldsForCreate();




    public function testGettingAllResources()
    {
        $totalCount = $this->queryModel->count();
        $crawler = $this->get($this->route);
        $crawler->assertJsonCount($totalCount);
    }

    public function testGettingSpecificResource()
    {
        $randomResource = $this->queryModel->inRandomOrder()->first();
        $crawler = $this->get($this->route . '/' . $randomResource->id);

        $crawler->assertJson($randomResource->toArray());
    }

    /**
     * @param $data
     */
    public function testCreatingANewResource()
    {
        $data = $this->providerFieldsForCreate();
        $crawler = $this->post($this->route, $data);
        $crawler->assertJson($data);
        $this->assertDatabaseHas($this->queryModel->getModel()->getTable(), $data);
    }

    /**
     * @param $data
     */
    public function testUpdatingAnExistingResource()
    {
        $data = $this->providerFieldsForCreate();
        $randomModel = $this->queryModel->inRandomOrder()->first();

        $crawler = $this->put($this->route . '/' . $randomModel->id, $data);

        $data['id'] = $randomModel->id;

        $this->assertDatabaseHas($this->queryModel->getModel()->getTable(), $data);
        $crawler->assertJson($data);
    }


    /**
     * @param $data
     */
    public function testDeletingAnExistingResource()
    {
        $data = $this->providerFieldsForCreate();
        $randomNewModel = $this->queryModel->create($data);
        $this->json('DELETE', $this->route . '/' . $randomNewModel->id);

        $this->assertDatabaseMissing($this->queryModel->getModel()->getTable(), ['id' => $randomNewModel->id]);
    }


}