<?php

namespace Tests\Feature\Api\Rest;

use App\Entities\Genre;
use Tests\Feature\Api\Rest\Traits\RestfullResourceTestTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GenresTest extends TestCase
{
    use RestfullResourceTestTrait;

    /**
     * Initialize the $queryModel property and defines the ideal cenario for running this test
     * @return mixed/
     */
    function havingIdealCenarioForTests()
    {
        $this->queryModel = Genre::query();
        $this->route = '/api/genres';
    }

    /**
     * Returns an array of data ready to be used as a input for create a new resource
     * @return array
     */
    function providerFieldsForCreate()
    {
        return ['genre_name' => 'RANDOM GENRE FOR THIS TEST'];
    }
}
