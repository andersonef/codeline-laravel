<?php

namespace Tests\Feature\Api\Rest;

use App\Entities\Country;
use App\Entities\Film;
use Tests\Feature\Api\Rest\Traits\RestfullResourceTestTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FilmsTest extends TestCase
{
    use RestfullResourceTestTrait;

    protected $country;

    /**
     * Initialize the $queryModel property and defines the ideal cenario for running this test
     * @return mixed/
     */
    function havingIdealCenarioForTests()
    {
        $this->queryModel = Film::query();
        $this->route = '/api/films';
        $this->country = factory(Country::class)->create();
    }

    /**
     * Returns an array of data ready to be used as a input for create a new resource
     * @return array
     */
    function providerFieldsForCreate()
    {
        return [
            'film_name'             => 'FILM NAME FOR RUNNING THIS TEST',
            'film_description'      => 'A GREAT FILM WITH A GREAT DESCRIPTION',
            'film_release_date'     => date('Y-m-d'),
            'film_ticket_price'     => 50.52,
            'film_photo'            => 'http://via.placeholder.com/1280x768',
            'country_id'            => $this->country->id,
        ];
    }
}
