<?php

namespace Tests\Feature\Api\Rest;

use App\Entities\Country;
use Tests\Feature\Api\Rest\Traits\RestfullResourceTestTrait;
use Faker\Generator;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CountriesTest extends TestCase
{
    use RestfullResourceTestTrait;



    /**
     * Initialize the $queryModel and $route properties and defines the ideal cenario for running this test
     * @return mixed/
     */
    function havingIdealCenarioForTests()
    {
        $this->queryModel = Country::query();
        $this->route = '/api/countries';
        factory(Country::class)->times(10)->create();
    }

    /**
     * Returns an array of data ready to be used as a input for create a new resource
     * @return array
     */
    function providerFieldsForCreate()
    {
        return ['country_name' => str_random(20)];
    }
}
