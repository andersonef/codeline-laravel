<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Country::class, function (Faker $faker) {
    return [
        'country_name' => $faker->name
    ];
});
