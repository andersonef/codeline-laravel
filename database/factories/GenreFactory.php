<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Genre::class, function (Faker $faker) {
    return [
        'genre_name'        => $faker->name
    ];
});
