<?php

use Faker\Generator as Faker;

$factory->define(\App\Entities\Film::class, function (Faker $faker) {
    $country = factory(\App\Entities\Country::class)->create();
    return [
        'film_name'             => $faker->name,
        'film_description'      => $faker->text(),
        'film_release_date'     => $faker->date('Y-m-d'),
        'film_ticket_price'     => $faker->randomFloat(2, 5, 50),
        'film_photo'            => 'http://via.placeholder.com/1280x768',
        'country_id'            => $country->id,
    ];
});
