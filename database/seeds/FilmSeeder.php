<?php

use Illuminate\Database\Seeder;

class FilmSeeder extends Seeder
{

    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = app(\Faker\Generator::class);

        $genresIds = \App\Entities\Genre::get(['id'])->pluck('id')->toArray();
        $films = factory(\App\Entities\Film::class)->times(500)->create();
        foreach($films as $film) {
            $film->Genres()->attach($this->faker->randomElements($genresIds, $this->faker->randomFloat(0, 1, count($genresIds)-1)));
        }
    }
}
