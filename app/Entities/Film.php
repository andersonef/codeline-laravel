<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{

    protected $fillable = ['country_id', 'film_name', 'film_description', 'film_release_date', 'film_ticket_price', 'film_photo'];

    public function Country()
    {
        return $this->hasOne(Country::class, 'country_id');
    }

    public function Genres()
    {
        return $this->belongsToMany(Genre::class, 'nxn_films_genres', 'film_id', 'genre_id' );
    }

}
