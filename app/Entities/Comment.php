<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['film_id', 'user_id', 'comment_text'];

    public function User()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    public function Film()
    {
        return $this->hasOne(Film::class, 'film_id');
    }
}
