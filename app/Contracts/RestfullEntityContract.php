<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 28/11/17
 * Time: 21:45
 */

namespace App\Contracts;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface RestfullEntityContract
{
    /**
     * Must return a new query from the entity this class will work on
     * @return Builder
     *
     */
    function provideEntityNewQuery();


    /**
     * Display a listing of the resource.
     *
     * @return Collection|static[]
     */
    public function index();

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create();



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Builder
     */
    public function store(Request $request);

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Builder
     */
    public function show($id);

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return void
     */
    public function edit($id);

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Model
     */
    public function update(Request $request, $id);

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id);




}