<?php
/**
 * Created by PhpStorm.
 * User: anderson
 * Date: 28/11/17
 * Time: 21:41
 */

namespace App\Traits;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Mockery\Exception\BadMethodCallException;

trait RestfullEntityControllerTrait
{
    abstract function provideEntityNewQuery();


    /**
     * Default exception raising for not restfull methods
     */
    private function badMethodException()
    {
        throw new BadMethodCallException('Restfull resource. This endpoint is not implemented');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Collection|static[]
     */
    public function index()
    {
        return $this->provideEntityNewQuery()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return $this->badMethodException();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Builder
     */
    public function store(Request $request)
    {
        $entity = $this->provideEntityNewQuery()->create($request->all());
        return $entity;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Builder
     */
    public function show($id)
    {
        return $this->provideEntityNewQuery()->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return void
     */
    public function edit($id)
    {
        return $this->badMethodException();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Model
     */
    public function update(Request $request, $id)
    {
        $entity = $this->provideEntityNewQuery()->find($id);
        $entity->update($request->all());
        return $entity->getModel();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entity = $this->provideEntityNewQuery()->find($id);
        $entity->delete();
    }
}