<?php

namespace App\Http\Controllers\Rest;

use App\Contracts\RestfullEntityContract;
use App\Entities\Country;
use App\Entities\Film;
use App\Traits\RestfullEntityControllerTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountriesController extends Controller implements RestfullEntityContract
{
    use RestfullEntityControllerTrait;

    /**
     * Must return a new query from the entity this class will work on
     * @return Builder
     *
     */
    function provideEntityNewQuery()
    {
        return Country::query();
    }
}
