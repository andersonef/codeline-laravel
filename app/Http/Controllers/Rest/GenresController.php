<?php

namespace App\Http\Controllers\Rest;

use App\Contracts\RestfullEntityContract;
use App\Entities\Genre;
use App\Traits\RestfullEntityControllerTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GenresController extends Controller implements RestfullEntityContract
{
    use RestfullEntityControllerTrait;

    /**
     * Must return a new query from the entity this class will work on
     * @return Builder
     *
     */
    function provideEntityNewQuery(): Builder
    {
        return Genre::query();
    }
}
